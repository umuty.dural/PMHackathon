import Vue from 'vue'
import VueApexCharts from 'vue-apexcharts'
Vue.config.productionTip = false


Vue.use(VueApexCharts)

Vue.component('apexchart', VueApexCharts)